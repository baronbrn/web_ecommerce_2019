<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserHasProductRepository")
 */
class UserHasProduct
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="productUser")
     */
    private $user;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="userProduct")
     */
    private $product;

    /**
     * @var $favoris
     * @ORM\Column(type="string", length=255)
     */
    private $favoris;

    /**
     * @var $owner
     * @ORM\Column(type="boolean")
     */
    private $owner;

    public function __construct()
    {
        $this->product = new ArrayCollection();
        $this->user = new ArrayCollection();
    }

    public function getUser(){ return $this->user; }
    public function setUser(Users $user){ $this->user = $user; return $this;}

    public function getProduct(){ return $this->product; }
    public function setProduct(Product $product){ $this->product = $product; return $this;}

    public function getFavoris(){ return $this->favoris; }
    public function setFavoris(string $favoris){ $this->favoris = $favoris; return $this;}

    public function isOwner(){ return $this->owner; }
    public function setOwner(bool $owner){ $this->owner = $owner; return $this;}
}
