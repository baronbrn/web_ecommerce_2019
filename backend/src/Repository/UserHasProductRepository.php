<?php

namespace App\Repository;

use App\Entity\Cart;
use App\Entity\UserHasProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserHasProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserHasProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserHasProduct[]    findAll()
 * @method UserHasProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserHasProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserHasProduct::class);
    }

    // /**
    //  * @return UserHasProduct[] Returns an array of CUserHasProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserHasProduct
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
