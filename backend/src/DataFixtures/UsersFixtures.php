<?php

namespace App\DataFixtures;

use App\Entity\Users;

class UsersFixtures extends AbstractFixtures
{
    public function load(\Doctrine\Persistence\ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        for ($i = 0; $i < 10; $i++) {
            /** @var Users $usr */
            $usr = new Users();
            $usr
                ->setEmail($this->faker->generateFaker()->email)
                ->setFirstName($this->faker->generateFaker()->firstName)
                ->setLastName($this->faker->generateFaker()->name)
                ->setCountry($this->faker->generateFaker()->country)
                ->setPassword($this->faker->generateFaker()->password)
                ->setOrders(null)
            ;
            $manager->persist($usr);
        }
        $manager->flush();
    }
}
