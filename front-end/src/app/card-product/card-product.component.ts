import { Component, OnInit, Input } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-card-product',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.css']
})
export class CardProductComponent implements OnInit {

  @Input() product;

  constructor(
    private _router: Router,
  ) { }

  ngOnInit(): void {
  }

  onProductClick() {
    this._router.navigate(['detail']);
  }

}
