import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private _router: Router,
  ) { }

  ngOnInit(): void {
  }

  onCategoriesClick() {
    this._router.navigate(['categories']);
  }

  onProductsClick() {
    this._router.navigate(['home']);
  }

  onConnexionClick() {
    this._router.navigate(['login']);
  }

}
