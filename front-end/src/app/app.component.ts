import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';
import { OnInit } from '@angular/core';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})

export class AppComponent {

//  title = "my-dream-app";

  constructor(
    public httpClient: HttpClient,
    private _router: Router,
  ) {}

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() {
    this._router.navigate(['home']);
  };

  listProduct = this.getProducts();

  getProducts() {
    var list = [];
    this.httpClient
      .get("http://localhost:8000/api/products")
      .subscribe(res => {
        res['hydra:member'].forEach(line => {
          list.push({
            id: line.id,
            name: line.name,
            description: line.description,
            picture: line.listingPicture
          });
        });
      });
    console.log(list);
    return list;
  }
}
