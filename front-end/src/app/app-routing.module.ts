import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConnexionComponent } from './connexion/connexion.component';
import {HomeComponent} from './home/home.component';
import {ProductDetailComponent} from './product-detail/product-detail.component';
import {CardCategoryComponent} from './card-category/card-category.component';


const routes: Routes = [
  { path: 'login', component: ConnexionComponent },
  { path: 'home', component: HomeComponent},
  { path: 'detail', component: ProductDetailComponent},
  { path: 'categories', component: CardCategoryComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingArrayComponnents = [
  ConnexionComponent,
  HomeComponent,
  ProductDetailComponent,
  CardCategoryComponent,
];
