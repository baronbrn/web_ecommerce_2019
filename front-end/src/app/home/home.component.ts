import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private _httpClient: HttpClient,
  ) { }

  public PRODUCT_URL = 'http://localhost:8000/api/products';


  ngOnInit(): void {
  }

  listProduct = this.getProducts();



  getProducts() {
    var productList = [];
    this._httpClient
      .get(this.PRODUCT_URL)
      .subscribe(res => {
        res['hydra:member'].forEach(line => {
          productList.push({
            id: line.id,
            name: line.name,
            description: line.description,
            picture: line.picture,
          });
        });
      });
    return productList;
  }



}
