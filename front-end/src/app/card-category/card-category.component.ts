import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-card-category',
  templateUrl: './card-category.component.html',
  styleUrls: ['./card-category.component.css']
})
export class CardCategoryComponent implements OnInit {

  constructor(
    private _httpClient: HttpClient,
  ) { }

  public CATEGORY_URL = 'http://localhost:8000/api/categories';

  ngOnInit(): void {
  }

  listCategory = this.getCategorie();

  getCategorie() {
    var categoryList = [];
    this._httpClient
      .get(this.CATEGORY_URL)
      .subscribe(res => {
        res['hydra:member'].forEach(line => {
          categoryList.push({
            id: line.id,
            name: line.name,
          });
        });
      });
    return categoryList;
  }

}
